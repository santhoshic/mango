defmodule MangoWeb.Plugs.FetchCartTest do
    use MangoWeb.ConnCase
    alias Mango.Sales
    alias Mango.Sales.Order

    test "Create and set cart on first visit" do
      conn = get build_conn(), "/"
      cart_id = get_session(conn, :cart_id)
      assert %Order{status: "In cart"} = conn.assigns.cart
      assert cart_id == conn.assigns.cart.id
    end

    test "Fetch and verify that the cart is same on different sessions" do
      conn = get build_conn(), "/"
      cart_id_1 = get_session(conn, :cart_id)

      conn = get conn, "/"

      assert  cart_id_1 == conn.assigns.cart.id
    end
end