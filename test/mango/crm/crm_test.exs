defmodule Mango.CRMTest do
  use Mango.DataCase
  alias Mango.CRM
  alias Mango.Repo
  alias Mango.CRM.Customer

  test "build_customer/0 returns and empty customer struct" do
    assert %Ecto.Changeset{data: %Customer{}} = CRM.build_customer
  end

  test "build_customer/1 return the struct with attribute data" do
    attrs = %{"name" => "john"}
    changeset = CRM.build_customer(attrs)
    assert changeset.params == attrs
  end

  test "create_customer/1 returns a customer if data is valid" do
    valid_attr = %{
    "name" => "santhosh",
    "email" => "s@g.com",
    "phone" => "111",
    "residence_area" => "Area 1",
    "password" => "1a2bc3"
    }

    assert {:ok, customer} = CRM.create_customer(valid_attr)
    assert Comeonin.Bcrypt.checkpw(valid_attr["password"],
    customer.password_hash)

  end

  test "create_customer/1 returns a changeset for invalid data" do
  invalid_attrs = %{}
  assert {:error, %Ecto.Changeset{}} = CRM.create_customer(invalid_attrs)
  (invalid_attrs)
  end

  test "get the customer provided email" do
        valid_attrs = %{
        "name" => "John",
        "email" => "john@example.com",
        "password" => "secret",
        "residence_area" => "Area 1",
        "phone" => "1111"
        }
        {:ok, customer1 } = CRM.create_customer(valid_attrs)
        customer2 = CRM.get_customer_by_email("john@example.com")

        assert customer1.id == customer2.id
  end

  test "get_customer_by_credentials" do
     valid_attrs = %{
     "name" => "John",
     "email" => "john@example.com",
     "password" => "secret",
     "residence_area" => "Area 1",
     "phone" => "1111"
     }
     {:ok, customer1 } = CRM.create_customer(valid_attrs)
     customer2 = CRM.get_customer_by_credential(valid_attrs)

     assert customer1.id == customer2.id
  end

  test "get customer by id when it exists" do
    valid_attrs = %{
         "name" => "John",
         "email" => "john@example.com",
         "password" => "secret",
         "residence_area" => "Area 1",
         "phone" => "1111"
         }
    {:ok, customer1} = CRM.create_customer(valid_attrs)
        customer2 = CRM.get_customer_by_id(customer1.id)

    assert customer1.id == customer2.id
  end

end