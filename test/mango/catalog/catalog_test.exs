defmodule Mango.CatalogTest do
  #use ExUnit.Case
  use Mango.DataCase
  alias Mango.{Catalog, Repo}
  alias Mango.Catalog.Product

 setup do
    Repo.insert %Product{ name: "Tomato", price: 50, sku: "A123",
    is_seasonal: false, category: "vegetables" }
    Repo.insert %Product{ name: "Apple", price: 100, sku: "B232",
    is_seasonal: true, category: "fruits" }
    :ok
end

  test "list_products/0 returns all products" do
    [p1 = %Product{}, p2 = %Product{}] = Catalog.list_products
    assert p1.name == "Tomato"
    assert p2.name == "Apple"
  end

  test "list_seasonal_products/0 returns products that are seasonal" do
    [p1 = %Product{}] = Catalog.list_seasonal_products
    assert p1.name == "Apple"
  end

  test "get_category_rpoducts/0 returns the category products" do
    [p1 = %Product{}] = Catalog.get_category_products("fruits")
    assert p1.name == "Apple"

    [p2 = %Product{}] = Catalog.get_category_products("vegetables")
    assert p2.name == "Tomato"
  end

end