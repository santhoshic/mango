
defmodule LoginTest do
    use Mango.DataCase
    use Hound.Helpers

    hound_session()

    setup do
        alias Mango.CRM
        valid_attrs = %{
        "name" => "John",
        "password" => "password",
        "email" => "j@a.com",
        "residence_area" => "Area 1",
        "phone" => "1111"
        }

        {:ok, _} = CRM.create_customer(valid_attrs)
        :ok
    end

    test "On successful login user is redirected to the \"/\" " do
        navigate_to("/login")
        form = find_element(:id, "session-form")

        find_within_element(form, :name, "session[email]")
        |> fill_field("J@a.com")

        find_within_element(form, :name, "session[password]")
        |> fill_field("password")

        find_within_element(form, :tag, "button")
        |> click()

        assert current_path == "/"
        message = find_element(:class, "alert-info")
        |> visible_text

        assert message == "Login successful"
    end

    test "shows error message on invalid login" do
        navigate_to("/login")
        form = find_element(:id, "session-form")
        find_within_element(form, :tag, "button")
        |> click()

        assert_path = "/login"
        message = find_element(:class, "alert-danger")
        |> visible_text()

        assert message == "Invalid username/password"

    end

end