defmodule MangoWeb.HomePageTest do
  #use ExUnit.Case
  use Mango.DataCase
  use Hound.Helpers
  alias Mango.{Repo, Catalog}
  alias Mango.Catalog.Product

  hound_session()

      setup do
          Repo.insert %Product{ name: "Tomato", price: 50, sku: "A123",
          is_seasonal: false, category: "vegetables" }
          Repo.insert %Product{ name: "Apple", price: 100, sku: "B232",
          is_seasonal: true, category: "fruits" }
          :ok
      end

  test "presence of featured products" do
    navigate_to("/")
    assert page_source() =~ "Seasonal products"
  end

  test "presence of seasonal products" do
    navigate_to("/")

    page_title = find_element(:css, ".page_title") |> visible_text()
    assert page_title == "Seasonal products"

    product = find_element(:css, ".product")
    product_name = find_within_element(product, :css, ".product_name") |> visible_text()
    product_price = find_within_element(product, :css, ".product_price") |> visible_text()

    assert product_name == "Apple"
    assert product_price == "100"

    refute page_source() =~ "Tomato"
  end
end