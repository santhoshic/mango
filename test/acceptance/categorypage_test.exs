defmodule MangoWeb.Acceptance.CategoryPagetTest do
    #use ExUnit.Case
    use Mango.DataCase
    use Hound.Helpers
    alias Mango.{Repo, Catalog}
    alias Mango.Catalog.Product

    hound_session()



    setup do
        Repo.insert %Product{ name: "Tomato", price: 50, sku: "A123",
        is_seasonal: false, category: "vegetables" }
        Repo.insert %Product{ name: "Apple", price: 100, sku: "B232",
        is_seasonal: true, category: "fruits" }
        :ok
    end

    test "show fruits" do
        navigate_to("/categories/Fruits")

        page_title = find_element(:css, ".page_title") |> visible_text

        assert page_title == "Fruits"

        product = find_element(:css, ".product")

        product_name = find_within_element(product, :css, ".product_name") |> visible_text
        product_price = find_within_element(product, :css, ".product_price") |> visible_text

        assert product_name == "Apple"
        assert product_price == "100"

        refute page_source() =~ "Tomato"
    end

    test "show vegetables" do
        navigate_to("/categories/Vegetables")

        page_title = find_element(:css, ".page_title") |> visible_text

        assert page_title == "Vegetables"

        product = find_element(:css, ".product")
        product_name = find_within_element(product, :css, ".product_name") |> visible_text
        product_price = find_within_element(product, :css, ".product_price") |> visible_text

        assert product_name == "Tomato"
        assert product_price == "50"

        refute page_source() =~ "Apple"
    end


end