defmodule Mango.Repo.Migrations.CreateProduct do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :price, :decimal
      add :is_seasonal, :boolean, default: false, null: false
      add :category, :string
      add :sku, :string
      add :image, :string
      add :pack_size, :string

      timestamps()
    end
    create unique_index(:products, [:sku])
  end
end
