defmodule MangoWeb.PageController do
  use MangoWeb, :controller
  alias Mango.Catalog

  def index(conn, _params) do
    seasonal_product = Catalog.list_seasonal_products
    render conn, "index.html", seasonal_products: seasonal_product
  end
end
