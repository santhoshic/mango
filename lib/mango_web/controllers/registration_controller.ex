defmodule MangoWeb.RegistrationController do
  use MangoWeb, :controller
  alias Mango.CRM
  alias Auroville.ResidenceService
  def new(conn, _params) do
    changeset = CRM.build_customer(%{name: "santhosh"})
    residence_areas = ResidenceService.list_areas()
    render(conn, "new.html", changeset: changeset, residence_areas: residence_areas)
  end

  def create(conn, %{"registration" => registration_data}) do
    case CRM.create_customer(registration_data) do
      {:ok, _customer} ->
        conn
        |> put_flash(:info, "Registration Successful")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset } ->
        residence_areas = ResidenceService.list_areas()
        conn
        |> render(:new, changeset: changeset, residence_areas: residence_areas)
      end
  end

end